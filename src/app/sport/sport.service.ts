import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sport } from './sport';

@Injectable({
  providedIn: 'root',
})

export class SportService {
  public sport: Sport[] = [];
  public balltype: string = '';
  public balldata: any;

  http = inject(HttpClient);

  /**
   * 取得表格內容
   */
  public async getdata():Promise<Sport[]>{
    const path = "/assets/mock/sport.json";
    let sports: any;
    sports = await this.http.get<Sport[]>(path).toPromise();
    this.sport = sports;
    console.log(this.sport);
    return sports;
  }

  /**
   * 取得籃球資料
   * @returns 籃球資料
   */
  public getBasketData(){
    this.balldata = this.sport;
    console.log(this.sport);
    this.balldata = this.balldata.filter((x: any) => {
      return x.balltype === "籃球";
    });
    this.sport = this.balldata;
  }
}


