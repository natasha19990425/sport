import { Component,inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SportTableComponent } from './sport-table/sport-table.component';
import { SportService } from './sport.service';
import { HttpClientModule } from '@angular/common/http'

//primeng
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';


@Component({
  selector: 'app-sport',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    SportTableComponent,
    //primeng
    ToolbarModule,
    ButtonModule,
    InputTextModule,
    HttpClientModule

  ],
  providers: [SportService],
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss']
})
export class SportComponent {
  public balltype: string = '';

  dataService = inject(SportService)

  ngOnInit(): void {}

  /**
   * 按下籃球按鈕時
   * 顯示籃球數據
   */
  public async onBasketClick() {
    this.dataService.getBasketData();
    this.balltype = '籃球'
  }

  /**
   * 按下棒球按鈕時
   * 顯示棒球數據
   */
  public async onBaseClick() { }

  /**
   * 按下足球按鈕時
   * 顯示足球數據
   */
  public async onSoccerClick() { }

  /**
   * 按下網球按鈕時
   * 顯示網球數據
   */
  public async onTennisClick() { }
}
