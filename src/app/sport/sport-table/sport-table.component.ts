import { SportService } from './../sport.service';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { Sport } from '../sport';

@Component({
  selector: 'app-sport-table',
  standalone: true,
  imports: [
    CommonModule,
    TableModule,
  ],
  providers: [SportService],
  templateUrl: './sport-table.component.html',
  styleUrls: ['./sport-table.component.scss']
})
export class SportTableComponent {
  public cols!: any[];
  public sport: Sport[] = [];

  dataService = inject(SportService);

  public async ngOnInit() {
    this.sport = await this.dataService.getdata();
  }
}
