export interface Sport {
  balltype: string;
  date: string;
  time: string;
  league: string;
  team: string;
  score: string;
}
